import React, { Component } from "react";

class VirtualDom extends Component {
  constructor() {
    super();
    this.state = {
      name: "Sachin",
    };
    setTimeout(() => {
      this.setState({
        name: "dhoni",
      });
    }, 5000);

    setTimeout(
      function () {
        this.setState({
          name: "kohli",
        });
      }.bind(this),
      10000
    );
  }
  render() {
    return (
      <div>
        <h1>5000lines of Code</h1>
        <h1>{this.state.name}</h1>
      </div>
    );
  }
}

export default VirtualDom;
