import React, { Component } from "react";

class TakeTheDataOnchange extends Component {
  constructor() {
    super();
    this.data = {};
  }

  fnChange = (e) => {
    let value = e.target.value;
    let id = e.target.id;
    this.data = {
      ...this.data,
      [id]: value,
    };
  };
  onSubmit = () => {
    console.log(this.data, "***");
  };
  render() {
    return (
      <div>
        <p>
          <b>Name</b>
          <input id="name" onChange={this.fnChange} />
        </p>
        <p>
          <b>Password</b>
          <input id="pwd" type="password" onChange={this.fnChange} />
        </p>
        <p>
          <b>Address</b>
          <input id="add" onChange={this.fnChange} />
        </p>
        <p>
          <button onClick={this.onSubmit}>Submit</button>
        </p>
      </div>
    );
  }
}

export default TakeTheDataOnchange;
