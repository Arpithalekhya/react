import React, { Component } from "react";
import myHoc from "./HOC";

class ButtonComp extends Component {
  render() {
    return (
      <div>
        <button onClick={this.props.f}>Count{this.props.c}</button>
      </div>
    );
  }
}

export default myHoc(ButtonComp);
