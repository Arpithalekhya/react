import React from "react";
import template from "./FilterNames.jsx";

class FilterNames extends React.Component {
  constructor() {
    super();
    this.myArray = [];
    this.state = {
      data: "",
      players: [],
      isBlue: false,
      id: "",
    };
  }
  fnClick = () => {
    let inputRef = this.refs.name;
    let value = inputRef.value;
    if (!this.myArray.includes(value)) {
      this.myArray.push(value);
    }
    this.setState({
      data: value,
      // players: [...this.state.players, value],
      players: this.myArray,
    });
    inputRef.value = "";
  };

  onFilterList = (e) => {
    let ID = e.target.id;
    this.setState({
      id: ID,
      isBlue: !this.state.isBlue,
    });
  };
  render() {
    return template.call(this);
  }
}

export default FilterNames;
