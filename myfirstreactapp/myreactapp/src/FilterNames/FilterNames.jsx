import "./FilterNames.css";
import React from "react";

function template() {
  return (
    <div className="filter-names">
      <input ref="name" />
      <button onClick={this.fnClick}>Click</button>
      <h1>Data:{this.state.data}</h1>
      {
        this.state.players.map((e, i) => {
          return <h1 id={i} onClick={this.onFilterList} style={{ color: this.state.isBlue && this.state.id == i ? "blue" : "" }}>{e}</h1>
        })
      }
    </div>
  );
};

export default template;
