import React, { Component } from "react";

class AjaxPromise extends Component {
  setHandleReq = (url) => {
    return new Promise((resolve, reject) => {
      let res;
      var obj = new XMLHttpRequest();
      obj.open("GET", url);
      obj.send();
      obj.onload = () => {
        res = obj.responseText;
        resolve(res);
      };
      obj.onerror = () => {
        res = obj.responseText;
        reject(res);
      };
    });
  };
  ajax = () => {
    this.setHandleReq("https://jsonplaceholder.typicode.com/posts")
      .then((r) => {
        console.log(r);
        alert(r);
      })
      .catch((r) => {
        console.log(r);
      });
  };

  render() {
    return (
      <div>
        <button onClick={this.ajax}>AjaxPromise</button>
      </div>
    );
  }
}

export default AjaxPromise;
