import React, { Component } from "react";

class TableResuable extends Component {
  render() {
    this.data = [
      { name: "Sachin", runs: "20" },
      { name: "Dhoni", runs: "30" },
      { name: "Kohli", runs: "40" },
    ];
    return (
      <>
        <table border="1px solid black">
          <thead>
            {this.props.h?.map((v, i) => {
              return <th key={i}>{v}</th>;
            })}
          </thead>
          <tbody>
            {this.props.d?.map((obj, i) => {
              return (
                <tr key={i}>
                  {Object.keys(obj)?.map((k, i) => {
                    return <td>{obj[k]}</td>;
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default TableResuable;
