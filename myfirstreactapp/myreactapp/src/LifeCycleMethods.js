import React, { Component } from "react";

class LifeCycleMethods extends Component {
  constructor(p) {
    super(p);
    debugger;
    this.state = {
      count: this.props.count,
    };
    console.log();
    console.log("constructor");
  }
  static getDerivedStateFromProps(prevProp, nextState) {
    debugger;
    console.log(" getDerivedStateFromProps");
    return null;
  }
  shouldComponentUpdate(nextProps, nextState) {
    debugger;
    console.log("shouldCOmponentUpdate");
    return true;
  }
  fnNameUpdate = () => {
    this.setState(
      {
        count: this.state.count + 1,
      },
      () => {
        if (this.state.count === 5) {
          this.props.fnName("Dhoni");
        }
      }
    );
  };

  render(a, b, c) {
    debugger;
    console.log("render");
    return (
      <div>
        <button onClick={this.fnNameUpdate}>
          Count:{this.state.count}
          <h1>I'm getting some props:{this.props.name}</h1>
        </button>
      </div>
    );
  }
  componentDidMount(a, b, c) {
    debugger;
    console.log("componentDidMOunt");
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    debugger;
    console.log("getSnapShotBeforeUpdate");
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    debugger;
    console.log("ComponentDidUpdate");
  }

  // componentWillUnmount(a, b, c) {
  //   debugger;
  //   alert("I'm unmounting");
  // }
  compentDidCatch(e) {
    alert(e);
  }
}
export default LifeCycleMethods;

//Mounting - loading component into the browser-4 methods
//Updating - any state or prop change will occur that time updating phase
//UnMounting - removing the component
//exception handling phase -to handle exceptions in react
