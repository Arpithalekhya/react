import React, { Component } from "react";

const myHoc = (Comp) => {
  class HOC extends React.Component {
    constructor() {
      super();
      this.state = {
        count: 0,
      };
    }
    onUpdateState = () => {
      this.setState({
        count: this.state.count + 1,
      });
    };
    render() {
      return (
        <div>
          <Comp c={this.state.count} f={this.onUpdateState} />
        </div>
      );
    }
  }
  return HOC;
};

export default myHoc;
