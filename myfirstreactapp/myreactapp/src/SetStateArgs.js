import React, { Component } from "react";

class SetStateArgs extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      count2: 0,
      count5: 0,
    };
  }
  onIncCount = () => {
    //setState is taking one arg object
    this.setState({
      count: this.state.count + 1,
    });
  };
  onIncCount2 = () => {
    //setState with first arg object and second arg callback function
    this.setState(
      {
        count2: this.state.count2 + 1,
      },
      () => {
        console.log(this.state.count2, "count2Value");
      }
    );
    console.log(this.state.count2, "count2ValueOUtside");
  };

  onIncCount5 = () => {
    //setState with callback as argument
    for (let i = 0; i < 5; i++) {
      this.setState((prev) => {
        return {
          count5: prev.count5 + 1,
        };
      });
    }
  };
  render() {
    return (
      <div>
        <button onClick={this.onIncCount}>Increament</button>
        <h1>Count:{this.state.count}</h1>

        <button onClick={this.onIncCount2}>IncreamentCount</button>
        <h1>Count:{this.state.count2}</h1>

        <button onClick={this.onIncCount5}>IncreamentCount5</button>
        <h1>Count:{this.state.count5}</h1>
      </div>
    );
  }
}

export default SetStateArgs;
