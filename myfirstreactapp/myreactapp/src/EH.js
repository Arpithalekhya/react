import React, { Component } from "react";
import Hero from "./Hero";
import ExceptionHandling from "./ExceptionHandling";

class EH extends Component {
  render() {
    return (
      <div>
        <ExceptionHandling>
          <Hero hero="Punith" />
        </ExceptionHandling>
        <ExceptionHandling>
          <Hero hero="Darshan" />
        </ExceptionHandling>
        <ExceptionHandling>
          <Hero hero="joker" />
        </ExceptionHandling>
      </div>
    );
  }
}

export default EH;
