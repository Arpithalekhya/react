import React, { Component } from "react";

class Hero extends Component {
  render() {
    if (this.props.hero === "joker") {
      throw new Error("error occured");
    }
    return <div>{this.props.hero}</div>;
  }
}

export default Hero;
