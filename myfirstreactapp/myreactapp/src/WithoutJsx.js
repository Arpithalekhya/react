import React, { Component, createElement } from "react";

class WithoutJsx extends Component {
  render() {
    return createElement("div", { id: "mainDiv", className: "myDiv" }, [
      createElement("div", null, "Hyderabd"),
      createElement(PrepareList, { name: "Sachin" }),
      createElement("h1", { style: { color: "red" } }, "Karnataka"),
    ]);
  }
}

class PrepareList extends React.Component {
  render() {
    return createElement("ul", null, [
      createElement("li", null, "Sachin"),
      createElement("li", null, "Dhoni"),
      createElement("h1", { style: { color: "blue" } }, this.props.name),
    ]);
  }
}

export default WithoutJsx;
