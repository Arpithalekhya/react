import React from "react";
import template from "./Menu.jsx";

class Menu extends React.Component {
  state = {
    name: "Sachin",
  };
  fnNameChange = (data) => {
    this.setState({
      name: data,
    });
  };
  getDerivedStateFromErrors() {
    console.log("GetDerivedStateFromErrors Got triggered");
  }
  fn = () => {
    this.setState({
      name: "Dhoni",
    });
  };
  render() {
    return template.call(this);
  }
}

export default Menu;
