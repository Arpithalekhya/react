import React, { lazy, Suspense } from "react"
import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import AjaxCallBack from "../AjaxCallback"
import AjaxPromise from "../AjaxPromise"
import "./Menu.css"
import AjaxAsyncAwait from "../AjaxAsyncAwait"
import FilterNames from "../FilterNames/FilterNames"
import SetStateArgs from "../SetStateArgs"
import LifeCycleMethods from "../LifeCycleMethods"
import ListAndKeys from "../ListAndKeys"
import ReusalbeComponents from "../ReusalbeComponents"
import ForwardRef from "../ForwardRef"
import TakeTheDataOnchange from "../takeTheDataOnchange"
import Context from "../Context"
import ButtonComp from "../ButtonComp"
import Hover from "../Hover"
import EH from "../EH"
import VirtualDom from "../VirtualDom"
const Withoutjsx = lazy(() => import("../WithoutJsx"))
const PureComponent = lazy(() => import("../PureComponent"))
const Portals = lazy(() => import("../Portals"))
const Profilers = lazy(() => import("../Profilers"))
const PropTypes = lazy(() => import("../PropTypes"))

function template() {
  return <div>
    <div>
      <Router>
        <div id="menu">
          <span id="link">
            <Link to="/">Home</Link>
          </span>
          <span id="link">
            <Link to="ajaxPromise">AjaxPromise</Link>
          </span>
          <span id="link">
            <Link to="ajaxAsync">AjaxAsync</Link>
          </span>
          <span id="link">
            <Link to="filterNames">FilterNames</Link>
          </span>
          <span id="link">
            <Link to="setState">SetState</Link>
          </span>
          <span id="link">
            <Link to="lifeCycleMethods">lifeCycleMethods</Link>
          </span>
          <span id="link">
            <Link to="listAndKeys">listAndKeys</Link>
          </span>
          <span id="link">
            <Link to="resuableComponents">Reusuable Components</Link>
          </span>
          <span id="link">
            <Link to="forwardRef">ForwardRef</Link>
          </span>
          <span id="link">
            <Link to="takeTheData">TakeDataOnchange</Link>
          </span>
          <span id="link">
            <Link to="context">Context</Link>
          </span>
          <span id="link">
            <Link to="btnCnt">ButtonComponent</Link>
          </span>
          <span id="link">
            <Link to="hover">HoverComponent</Link>
          </span>
          <span id="link">
            <Link to="EH">Exception Handling</Link>
          </span>
          <span id="link">
            <Link to="jsx">WithoutJSx</Link>
          </span>
          <span id="link">
            <Link to="pureComponent">PureComponent</Link>
          </span>
          <span id="link">
            <Link to="portals">Portals</Link>
          </span>
          <span id="link">
            <Link to="portals">Profilers</Link>
          </span>
          <span id="link">
            <Link to="propTypes">PropTypes</Link>
          </span>
          <span id="link">
            <Link to="vd">VirtualDOM</Link>
          </span>

        </div>
        <Suspense fallback="loading">
          <Route exact path="/" component={AjaxCallBack} />
          <Route path="/ajaxPromise"><AjaxPromise /></Route>
          <Route path="/ajaxAsync" render={() => <AjaxAsyncAwait />} />
          <Route path="/filterNames" render={() => <FilterNames />} />
          <Route path="/setState" render={() => <SetStateArgs />} />
          <Route path="/lifeCycleMethods" render={() => <LifeCycleMethods count={0} name={this.state.name} fnName={this.fnNameChange} />} />
          <Route path="/listAndKeys" component={ListAndKeys} />
          <Route path="/resuableComponents" component={ReusalbeComponents} />
          <Route path="/forwardRef" component={ForwardRef} />
          <Route path="/takeTheData" component={TakeTheDataOnchange} />
          <Route path="/context" component={Context} />
          <Route path="/btnCnt" component={ButtonComp} />
          <Route path="/hover" component={Hover} />
          <Route path="/eh" component={EH} />
          <Route path="/jsx" component={Withoutjsx} />
          <Route path="/pureComponent" render={() => <PureComponent />} />
          <Route path="/portals"><Portals /></Route>
          <Route path="/portals"><Profilers /></Route>
          <Route path="/propTypes"><PropTypes /></Route>
          <Route path="/vd" component={VirtualDom} />
        </Suspense>
      </Router>
    </div>
  </div >
}



export default template