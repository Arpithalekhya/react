import React, { Component } from "react";
//ref,ownRef=React.creatRef,onChange

class ForwardRef extends Component {
  constructor() {
    super();
    this.myRef = React.createRef(null);
    this.myRefFunctional = React.createRef(null);
  }
  onSetName = () => {
    this.refs.name.value = "Sachin";
    this.myRef.current.value = this.refs.name.value;
    this.myRefFunctional.current.value = "Hyderabad";
  };
  render() {
    return (
      <div>
        <input ref="name" />
        {/* <button onClick={() => (this.refs.name.value = "Sachin")}>Click</button> */}
        <button onClick={this.onSetName}>Click</button>
        <Child m={this.myRef} />
        <ChildFunction ref={this.myRefFunctional} name="Functional Component" />
      </div>
    );
  }
}

class Child extends React.Component {
  render() {
    return (
      <React.Fragment>
        <p>
          <input ref={this.props.m} />
        </p>
      </React.Fragment>
    );
  }
}

const ChildFunction = React.forwardRef(({ name }, ref) => {
  return (
    <React.Fragment>
      <input ref={ref} />
      {name}
    </React.Fragment>
  );
});

export default ForwardRef;
