import React from "react";

class AjaxCallback extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }
  setHandleReq = (url, cb) => {
    let res;
    var obj = new XMLHttpRequest();
    obj.open("GET", url);
    obj.send();
    obj.onload = () => {
      res = obj.responseText;
      cb(res);
    };
    obj.onerror = () => {
      res = obj.responseText;
      cb(res);
    };
  };
  ajax = () => {
    this.setHandleReq("https://jsonplaceholder.typicode.com/posts", (s) => {
      console.log(s);
      this.setState({
        data: s,
      });
    });
    // this.setHandleReq(
    //   "https://jsonplaceholder.typicode.com/posts",
    //   function f1(s) {
    //     alert(s);
    //   }
    // );
  };
  render() {
    return (
      <div>
        <button onClick={this.ajax}>AJAX</button>
        <h1>{this.state.data}</h1>
      </div>
    );
  }
}

export default AjaxCallback;
