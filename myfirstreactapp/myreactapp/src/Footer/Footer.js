import React from "react";
import template from "./Footer.jsx";

class Footer extends React.Component {
  render() {
    return template.apply(this);
  }
}

export default Footer;
