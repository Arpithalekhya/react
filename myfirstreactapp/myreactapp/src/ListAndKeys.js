import React, { Component } from "react";

class ListAndKeys extends Component {
  constructor() {
    super();
    this.players = ["Sachin", "Dhoni", "Kohli", "UV", "Panth"];
    this.heros = ["H", "S", "A", "L"];
    this.data = [
      { name: "Sachin", runs: 12 },
      { name: "Dhoni", runs: 10 },
      { name: "Kohli", runs: 20 },
      { name: "Virat", runs: 20 },
    ];
    this.headers = ["Name", "Runs"];
    this.colors = ["red", "blue", "green"];
    this.sachinDetails = {
      name: "Sachin",
      runs: 20000,
      location: "Mumbai",
      fullName: "ST",
    };
  }
  render() {
    return (
      <div>
        <table border="1px solid black">
          <tr>
            {this.headers.map((h, i) => {
              return <th key={i}>{h}</th>;
            })}
          </tr>
          <tbody>
            {this.data.map((obj, i) => {
              return (
                <tr>
                  <td>{obj.name}</td>
                  <td>{obj.runs}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <h1>Players List</h1>
        <select>
          {this.heros.map((v, i) => {
            return <option key={i}>{v}</option>;
          })}
        </select>
        {this.players.map((v, i) => {
          return <h1 key={i}>{v}</h1>;
        })}
        {this.players.map((v, i) => {
          return <span key={i}>{v}</span>;
        })}
        <ul>
          {this.players.map((v, i) => {
            return <li>{v}</li>;
          })}
        </ul>
        <ol>
          {this.players.map((v, i) => {
            return <li>{v}</li>;
          })}
        </ol>
        {this.colors.map((e, i) => {
          return <h1 key={i}>{e}</h1>;
        })}

        <h1>Preparing Table From Object Data</h1>
        <table border="1px solid black">
          <tbody>
            {Object.keys(this.sachinDetails).map((objectKeys, i) => {
              return (
                <tr>
                  <td>{objectKeys}</td>
                  <td>{this.sachinDetails[objectKeys]}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ListAndKeys;
