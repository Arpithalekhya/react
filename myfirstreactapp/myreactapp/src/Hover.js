import React, { Component } from "react";
import myHoc from "./HOC";

class Hover extends Component {
  render() {
    return (
      <div>
        <h1 onMouseOver={this.props.f}>Hover{this.props.c}</h1>
      </div>
    );
  }
}

export default myHoc(Hover);
