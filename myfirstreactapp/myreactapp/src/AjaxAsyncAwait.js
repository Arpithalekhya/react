import React, { Component } from "react";

class AjaxAsyncAwait extends Component {
  setHandleReq = (url) => {
    return new Promise((resolve, reject) => {
      let res;
      var obj = new XMLHttpRequest();
      obj.open("GET", url);
      obj.send();
      obj.onload = () => {
        res = obj.responseText;
        resolve(res);
      };
      obj.onerror = () => {
        res = obj.responseText;
        reject(res);
      };
    });
  };
  ajax = async () => {
    let res = await this.setHandleReq(
      "https://jsonplaceholder.typicode.com/posts"
    );
    alert(res);
  };

  render() {
    return (
      <div>
        <button onClick={this.ajax}>AjaxAsyncAwait</button>
      </div>
    );
  }
}

export default AjaxAsyncAwait;
