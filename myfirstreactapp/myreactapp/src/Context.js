import React, { Component } from "react";

const ctx = React.createContext();
class Context extends Component {
  render() {
    return (
      <div>
        <h1>I'm Context</h1>
        <ctx.Provider value={{ n: "sachin", l: "Hyderabad" }}>
          <A />
        </ctx.Provider>
      </div>
    );
  }
}

class A extends Component {
  render() {
    return (
      <div>
        <h1>
          I'm A::::::<ctx.Consumer>{(data) => data.n}</ctx.Consumer>
        </h1>
        <B />
      </div>
    );
  }
}
class B extends Component {
  render() {
    return (
      <div>
        <h1>I'm B:::</h1>
        <C />
      </div>
    );
  }
}
class C extends Component {
  render() {
    return (
      <div>
        <h1>I'm C</h1>
        <D />
      </div>
    );
  }
}
class D extends Component {
  render() {
    return (
      <div>
        <h1>I'm D</h1>
        <E />
      </div>
    );
  }
}
class E extends Component {
  render() {
    return (
      <b>
        Data::::
        <ctx.Consumer>
          {({ n, l }) => {
            return (
              <span>
                {n},,,,,,
                {l}
              </span>
            );
          }}
        </ctx.Consumer>
      </b>
    );
  }
}

export default Context;
