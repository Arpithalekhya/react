import React, { Component } from "react";

class DropDownLikeReusuable extends Component {
  render() {
    return (
      <div>
        <select>
          {this.props.data?.map((e, i) => {
            return <option key={i}>{e}</option>;
          })}
        </select>
      </div>
    );
  }
}

export default DropDownLikeReusuable;

export const name = "React";
export const location = "Hyderabad";
