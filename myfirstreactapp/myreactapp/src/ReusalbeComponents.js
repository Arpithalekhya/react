import React, { Component } from "react";
import ListLikeResuable from "./ListLikeResuable";
import DropDownLikeReusuable, {
  name as n,
  location,
} from "./dropDownLikeReusuable";
import InputControlReusuable from "./inputControlReusuable";
import TableResuable from "./TableResuable";

class ReusalbeComponents extends Component {
  constructor() {
    super();
    this.headers = ["Name", "Runs"];
    this.data = [
      { name: "Sachin", runs: "20" },
      { name: "Dhoni", runs: "30" },
      { name: "Kohli", runs: "40" },
    ];
  }
  render() {
    return (
      <div>
        <ListLikeResuable data={["red", "blue", "green"]} />
        <ListLikeResuable data={["1", "2", "3"]} />
        <DropDownLikeReusuable data={["sachin", "dhoni", "kohli"]} />
        <DropDownLikeReusuable data={["1", "2", "3"]} />
        <h1> Name:{n}</h1>
        <h2>{location}</h2>
        Gender <InputControlReusuable type="radio" />
        Male
        <InputControlReusuable type="radio" />
        Female
        <TableResuable h={this.headers} d={this.data} />
        <TableResuable
          h={["color", "flowers"]}
          d={[
            { color: "red", flower: "jasmin" },
            { color: "blue", flower: "Hibiscus" },
          ]}
        />
        <h1>ddd</h1>
      </div>
    );
  }
}

export default ReusalbeComponents;
