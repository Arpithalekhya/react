import React, { Component, Profiler } from "react";
import Test from "./Test";

class Profilers extends Component {
  constructor() {
    super();
    this.state = {
      name: "sachin",
    };
    setTimeout(() => {
      this.setState({
        name: "Dhoni",
      });
    }, 5000);
  }
  fnRender = (
    id,
    phase,
    actualDuration,
    baseDuration,
    startTime,
    commitTime,
    interactions
  ) => {
    debugger;
    console.log(id, "id");
    console.log(phase, "phase");
    console.log(actualDuration, "actualDuration");
    console.log(baseDuration, " baseDuration");
    console.log(startTime, "startTime");
    console.log(commitTime, " commitTime");
    console.log(interactions, "interactions");
  };
  render() {
    return (
      <div>
        <Profiler id="test" onRender={this.fnRender}>
          <Test name={this.state.name} />
        </Profiler>
      </div>
    );
  }
}

export default Profilers;
