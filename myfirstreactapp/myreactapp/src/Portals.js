import React, { useRef, useState } from "react";

function PrimeNumbers() {
  const primeRef = useRef(null);
  const [arr, setArr] = useState([]);
  const getPrime = () => {
    debugger;
    const number = Number(primeRef.current.value);
    if (number > 1) {
      let myarr = [];
      for (let i = 2; i < number; i++) {
        if (number % i === 0) {
          myarr.push(i);
        }
        setArr(myarr);
      }
    }
  };

  return (
    <div>
      <input ref={primeRef} />
      <button onClick={getPrime}>GetPrime</button>
      {arr.map((a) => {
        return <div>{a}</div>;
      })}
    </div>
  );
}

export default PrimeNumbers;
