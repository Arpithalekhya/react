import React, { Component } from "react";
import PropType from "prop-types";

class PropTypes extends Component {
  render() {
    return (
      <div>
        <Child data={{ name: "sachin" }} />
      </div>
    );
  }
}

class Child extends Component {
  render() {
    return <div>{this.props.data.length}</div>;
  }
}
Child.protoType = {
  data: PropType.string,
};

export default PropTypes;
