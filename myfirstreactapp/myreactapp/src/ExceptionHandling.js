import React, { Component } from "react";

class ExceptionHandling extends Component {
  constructor() {
    super();
    this.state = {
      isError: false,
    };
  }
  static getDerivedStateFromError() {
    return {
      isError: true,
    };
  }
  componentDidCatch(e) {
    // alert(e);
  }
  render() {
    console.log(this, "EXCEPTION HANDLING");
    return (
      <div>
        {this.state.isError ? "Something Went wrong" : this.props.children}
      </div>
    );
  }
}

export default ExceptionHandling;
