import React, { Component } from "react";

class ListLikeResuable extends Component {
  render() {
    console.log(this, "thisProps");
    return (
      <div>
        <ul>
          {/* {this.props.players &&
            this.props.players.map((v, i) => {
              return <li>{v}</li>;
            })} */}
          {this.props.data?.map((v, i) => {
            return <li>{v}</li>;
          })}
        </ul>
      </div>
    );
  }
}

export default ListLikeResuable;
