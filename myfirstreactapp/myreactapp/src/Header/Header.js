import React from "react";
import template from "./Header.jsx";
import "./Header.css";

class Header extends React.Component {
  render() {
    return template.call(this);
  }
}
export default Header;
