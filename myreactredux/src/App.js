import "./App.css";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Menu from "./Components/Menu";
import {Component} from "react";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Menu />
        <Footer />
      </div>
    );
  }
}

export default App;
