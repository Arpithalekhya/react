import React, {useState, memo, useCallback} from "react";

function MyuseCallBack(props) {
  const [count, setCount] = useState(0);
  const [name, setName] = useState();
  const fnClick = useCallback(() => {
    setCount(count + 1);
  }, [count]);
  return (
    <div>
      <p>
        {" "}
        <input onChange={(e) => setName(e.target.value)} />
      </p>
      <button onClick={fnClick}>INC COUNT</button>
      <h1>{count}</h1>
      <Child count={count} fnClick={fnClick} />
    </div>
  );
}

const Child = memo(({count, fnClick}) => {
  alert("child Called");
  return (
    <div>
      I'm Child
      <h1>{count}</h1>
      <h1>
        <button onClick={fnClick}>CHILD</button>
      </h1>
    </div>
  );
});

export default MyuseCallBack;
