import React, { useState } from "react";

// class useState1 extends Component {
//   constructor() {
//     super();
//     this.myRef = React.createRef();
//   }
//   fnSetName = () => {
//     this.refs.name.value = "Sachin";
//   };
//   componentDidMount() {
//     this.myRef.current.value = "Soumya";
//   }
//   render() {
//     return (
//       <div>
//         <input ref="name" />
//         <input ref={this.myRef} />
//         <button onClick={this.fnSetName}>Click Me</button>
//       </div>
//     );
//   }
// }

// export default useState1;

const SetName = () => {
  const [name, setMyName] = useState("Sachin");
  const myRef = React.createRef(null);
  const inputRef = React.createRef(null);
  const setName = () => {
    myRef.current.value = "Soumya";
    inputRef.current.focus();
  };
  const setFocus = () => {
    inputRef.current.focus();
  };
  const nameChange = () => {
    setMyName("Dhoni");
  };
  return (
    <div>
      <input ref={myRef} />
      <input ref={inputRef} />
      <button onClick={setName}>Click me</button>
      <button onClick={setFocus}>SetFocus</button>
      <button onClick={nameChange}>nameChange</button>
      <h1>{name}</h1>
    </div>
  );
};

export default SetName;
