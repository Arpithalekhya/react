import React, { useRef } from "react";

function MyUseRef(props) {
  //   const myRef = React.createRef(null);
  const myRef = useRef(null);
  const myOwnRef = useRef(null);

  const fnClick = () => {
    myRef.current.value = "Sachin";
    myOwnRef.current.innerText = "Dubai";
  };
  return (
    <div>
      MyUseRef
      <Child r={myRef} />
      <button onClick={fnClick}>Click me</button>
      <h2 ref={myOwnRef}>Hyderabad</h2>
    </div>
  );
}
const Child = (data) => {
  return (
    <div>
      <input ref={data.r} />
    </div>
  );
};
export default MyUseRef;
