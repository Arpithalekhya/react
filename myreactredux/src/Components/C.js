import React, { Component } from "react";
import { connect } from "react-redux";

class C extends Component {
  render() {
    return (
      <div>
        <h1>Name:{this.props.myN}</h1>
        <h1>Location:{this.props.myL}</h1>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    myN: state.m.name,
    myL: state.m.location,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(C);
