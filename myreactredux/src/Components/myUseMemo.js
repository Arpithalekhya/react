import {useState, useMemo} from "react";

const MyUseMemo = () => {
  const [count, setCount] = useState(0);
  const o = useMemo(() => {
    let output = 0;
    for (let i = 0; i < 50000000; i++) {
      output++;
    }
    return output;
  }, []);
  const fnClick = () => {
    setCount(count + 1);
  };
  return (
    <div>
      <button onClick={fnClick}>Click me</button>
      <h1>{count}</h1>
      <h1>{o}</h1>
    </div>
  );
};
export default MyUseMemo;
