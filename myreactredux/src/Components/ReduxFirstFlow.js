import React, { Component } from "react";
import A from "./A";
import B from "./B";
import C from "./C";

class ReduxFirstFlow extends Component {
  render() {
    return (
      <div>
        <A />
        <B />
        <C />
      </div>
    );
  }
}

export default ReduxFirstFlow;
