import React, { Component } from "react";
import myStore from "../Store/store";

class B extends Component {
  onLocation = () => {
    debugger;
    let location = this.refs.loc.value;
    myStore.dispatch({
      type: "LOC",
      payload: location,
    });
  };
  render() {
    return (
      <p>
        <input ref="loc" />
        <button onClick={this.onLocation}>Location</button>
      </p>
    );
  }
}
export default B;
