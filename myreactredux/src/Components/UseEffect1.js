import { useEffect, useState } from "react";

export const LifeCycleMethodsFunctional1 = () => {
  const [name, setName] = useState("Sachin");
  const [count, setCount] = useState(0);
  const fnNameChange = () => {
    setName("DHONI");
  };
  const incCount = () => {
    setCount(count + 1);
  };

  useEffect(() => {
    console.log("compnentDidMount,useEffectwithEmptyDep");
  }, []);

  useEffect(() => {
    console.log("compoenntDidUpdate", "useEffectwithoutanyDep", "NAME");
  }, [name]);

  useEffect(() => {
    console.log("compoenntDidUpdate", "useEffectwithoutanyDep", "COUNT");
  }, [count]);
  return (
    <div>
      useEffect
      <button onClick={fnNameChange}>Click Me</button>
      <h1>{name}</h1>
      <h1>
        <button onClick={incCount}>COunt</button>
        <p>{count}</p>
      </h1>
    </div>
  );
};
