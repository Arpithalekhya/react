import React, {Component, lazy, Suspense} from "react";
import {Routes, Route, Link, HashRouter} from "react-router-dom";
import ReduxFirstFlow from "../Components/ReduxFirstFlow";
import ReduxSaga from "./ReduxSaga";
import {LifeCycleMethodsFunctional1} from "../Components/UseEffect1";
import UseContext from "./UseContext";
const ReduxThunk = lazy(() => import("./ReduxThunk"));
const ReduxAjax = lazy(() => import("./ReduxAjax"));
const UseState = lazy(() => import("./UseState"));
const UseState1 = lazy(() => import("./useState1"));
const LifeCycleMethodsFunctional = lazy(() => import("./UseEffect"));
const LifeCycleMethodsFunctional2 = lazy(() => import("./useEffect3"));
const MyUseRef = lazy(() => import("./useRef"));
const MyUseReducer = lazy(() => import("./useReducer"));
const MyUseReducer1 = lazy(() => import("./UseReducer1"));
const MyUseMemo = lazy(() => import("./myUseMemo"));
const MyuseCallBack = lazy(() => import("./MyuseCallBack"));
const ReactMemo = lazy(() => import("./ReactMemo"));
const ReactMemo1 = lazy(() => import("./ReactMemo2"));

class Menu extends Component {
  render() {
    const routeData = [
      {
        path: "/reduxFirstFlow",
        element: <ReduxFirstFlow />
      },
      {
        path: "/reduxSaga",
        element: <ReduxSaga />
      },
      {
        path: "/reduxThunk",
        element: <ReduxThunk />
      },
      {
        path: "/reduxAjax",
        element: <ReduxAjax />
      },
      {
        path: "/reduxAjax",
        element: <ReduxAjax />
      },
      {
        path: "/useState",
        element: <UseState />
      },
      {
        path: "/useState1",
        element: <UseState1 />
      },
      {
        path: "/useEffect",
        element: <LifeCycleMethodsFunctional />
      },
      {
        path: "/useEffect1",
        element: <LifeCycleMethodsFunctional1 />
      },
      {
        path: "/useEffect2",
        element: <LifeCycleMethodsFunctional2 />
      },
      {
        path: "/useRef",
        element: <MyUseRef />
      },
      {
        path: "/useContext",
        element: <UseContext />
      },
      {
        path: "/useReducer",
        element: <MyUseReducer />
      },
      {
        path: "/useReducer1",
        element: <MyUseReducer1 />
      },
      {
        path: "/useMemo",
        element: <MyUseMemo />
      },
      {
        path: "/useCallBack",
        element: <MyuseCallBack />
      },
      {
        path: "/reactMemo",
        element: <ReactMemo />
      },
      {
        path: "/reactMemo1",
        element: <ReactMemo1 />
      }
    ];
    const linkData = [
      {
        to: "/reduxFirstFlow",
        content: "ReduxFirstFlow"
      },
      {
        to: "/reduxSaga",
        content: "ReduxSaga"
      },
      {
        to: "/reduxThunk",
        content: "ReduxThunk"
      },
      {
        to: "/reduxAjax",
        content: "ReduxAjax"
      },
      {
        to: "/useState",
        content: "UseState"
      },
      {
        to: "/useState1",
        content: "UseState1"
      },
      {
        to: "/useEffect",
        content: "UseEffect"
      },
      {
        to: "/useEffect1",
        content: "UseEffect1"
      },
      {
        to: "/useEffect2",
        content: "UseEffect2"
      },
      {
        to: "/useRef",
        content: "USEREF"
      },
      {
        to: "/useContext",
        content: "USECONTEXT"
      },
      {
        to: "/useReducer",
        content: "USEREDUCER"
      },
      {
        to: "/useReducer1",
        content: "USEREDUCER1"
      },
      {
        to: "/useMemo",
        content: "USEMEMO"
      },
      {
        to: "/useCallBack",
        content: "USECALLBACK"
      },
      {
        to: "/reactMemo",
        content: "ReactMEMO"
      },
      {
        to: "/reactMemo1",
        content: "ReactMEMO1"
      }
    ];
    return (
      <div>
        <HashRouter>
          {linkData.map((obj) => {
            return (
              <span style={{padding: 5}}>
                {" "}
                <Link to={obj.to}>{obj.content}</Link>
              </span>
            );
          })}
          <Suspense fallback="....Loading">
            <Routes>
              {routeData.map((obj) => {
                return <Route path={obj.path} element={obj.element} />;
              })}
            </Routes>
          </Suspense>
        </HashRouter>
      </div>
    );
  }
}

export default Menu;
