import { useState } from "react";

const IncrementCount = () => {
  const [count, s] = useState(10);
  const fnClick = () => {
    s(count + 1);
  };
  return (
    <div>
      <button onClick={fnClick}>Click me</button>
      <h1>{count}</h1>
    </div>
  );
};

export default IncrementCount;
