import { useEffect, useState } from "react";

const LifeCycleMethodsFunctional2 = () => {
  const [show, setShow] = useState(true);
  const hideShow = () => {
    setShow(!show);
  };
  return (
    <div>
      UseEffect as a COmponentWillUnmount
      <h1>
        {show && <Child />}
        <button onClick={hideShow}>UnMount</button>
      </h1>
    </div>
  );
};

const Child = () => {
  useEffect(() => {
    return () => {
      alert("componentWillUnmount");
    };
  }, []);
  return <h1>I'm a Child Component</h1>;
};
export default LifeCycleMethodsFunctional2;
