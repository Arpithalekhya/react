import React, {useContext, useReducer, useRef} from "react";

const ctx = React.createContext();

const myReducer = (state, action) => {
  debugger;
  switch (action.type) {
    case "NAME":
      return {
        ...state,
        name: action.payload
      };
    case "LOC":
      return {
        ...state,
        location: action.payload
      };
    default:
      break;
  }
  return state;
};
const initiaData = {
  name: "",
  location: ""
};

const UseReducer1 = () => {
  const [myState, myDispatch] = useReducer(myReducer, initiaData);
  return (
    <div>
      <ctx.Provider value={{state: myState, dispatch: myDispatch}}>
        <A />
        <B />
        <C />
      </ctx.Provider>
    </div>
  );
};

export default UseReducer1;

const A = () => {
  const data = useContext(ctx);
  const nameRef = useRef(null);
  const fnName = () => {
    data.dispatch({
      type: "NAME",
      payload: nameRef.current.value
    });
  };
  return (
    <p>
      <input ref={nameRef} />
      <button onClick={fnName}>Name</button>
    </p>
  );
};
const B = () => {
  const data = useContext(ctx);
  const locationRef = useRef();
  const fnLocation = () => {
    let location = locationRef.current.value;
    data.dispatch({
      type: "LOC",
      payload: location
    });
  };
  return (
    <p>
      <input ref={locationRef} />
      <button onClick={fnLocation}>Location</button>
    </p>
  );
};
const C = () => {
  const data = useContext(ctx);
  return (
    <>
      <h1>Name:{data.state.name}</h1>
      <h1>Location:{data.state.location}</h1>
    </>
  );
};
