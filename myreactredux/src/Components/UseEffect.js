import React, { useEffect, useState } from "react";

const LifeCycleMethodsFunctional = () => {
  const [myData, setData] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts", {
      method: "GET",
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => setData(data));
  }, []);

  return (
    <div>
      {myData.map((obj) => {
        return <h1>{obj.title}</h1>;
      })}
    </div>
  );
};
export default LifeCycleMethodsFunctional;

//useEffect(()=>{},[])(FC) ==componentDidMount(CC)
//useEffect(()=>{}) ==componentDidUpdate
//useEffect(()=>{return ()=>{console.log(123)}}) ==componentwillUnmount(CC)
