import React, { Component } from "react";
import { connect } from "react-redux";

class ReduxAjax extends Component {
  state = {
    myTableData: [],
  };
  fnClick = async () => {
    let res = await fetch("https://jsonplaceholder.typicode.com/posts", {
      method: "GET",
    }).then((res) => res.json());
    console.log(res);
    this.props.disp({
      type: "DATA",
      d: res,
    });
  };
  fnGetStoreData = () => {
    this.setState({
      myTableData: this.props.data,
    });
  };
  render() {
    return (
      <div>
        <button onClick={this.fnClick}>Ajax</button>
        <button onClick={this.fnGetStoreData}>TOGETSTOREDATA</button>
        <table border="1px solid black">
          {this.state.myTableData.map((obj) => {
            return (
              <tr>
                <td>{obj.id}</td>
                <td>{obj.title}</td>
              </tr>
            );
          })}
        </table>
      </div>
    );
  }
}
const mdp = (d) => {
  return {
    disp: d,
  };
};

const msp = (state) => {
  return {
    data: state.m.data,
  };
};

export default connect(msp, mdp)(ReduxAjax);
