// import React, {Component, PureComponent} from "react";

// class ReactMemo2 extends Component {
//   state = {
//     name: "Sachin",
//     count: 0
//   };
//   constructor() {
//     super();
//     setTimeout(() => {
//       this.setState({
//         count: 10
//       });
//     }, 5000);
//   }
//   fnClick() {
//     this.setState({
//       name: "Dhoni"
//     });
//   }
//   render() {
//     alert("Parent Called");
//     return (
//       <div>
//         <Child name={this.state.name} />
//         <button onClick={this.fnClick.bind(this)}>Click me</button>
//       </div>
//     );
//   }
// }

// export default ReactMemo2;

// class Child extends PureComponent {
//   render() {
//     alert("Child Called");
//     return <h1>{this.props.name}</h1>;
//   }
// }

import React, {useState, memo} from "react";

function ReactMemo2(props) {
  alert("Parent Called");
  const [name, setName] = useState("Sachin");
  const fnClick = () => {
    setName("Sachin");
  };
  return (
    <div>
      <button onClick={fnClick}>CLick Me</button>
      <h1>{name}</h1>
      <Child name={name} />
    </div>
  );
}

export default ReactMemo2;

const Child = memo(({name}) => {
  alert("child Called");
  return <h2>I'm Child{name}</h2>;
});
