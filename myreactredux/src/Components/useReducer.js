import React, {useReducer} from "react";

const myReducer = (state, action) => {
  if (action.type == "COUNT") {
    return {
      ...state,
      count: state.count + 1
    };
  }
  if (action.type == "CHANGENAME") {
    return {
      ...state,
      name: "DHONI"
    };
  }
  return state;
};
const initiaData = {
  count: 0,
  name: "Sachin"
};

function MyUseReducer(props) {
  const fnClick = () => {
    dispatch({
      type: "COUNT"
    });
  };
  const fnNameChange = () => {
    dispatch({
      type: "CHANGENAME"
    });
  };
  const [state, dispatch] = useReducer(myReducer, initiaData);
  return (
    <div>
      <button onClick={fnClick}>Click me</button>
      <button onClick={fnNameChange}>Click to Change Name</button>
      <h1>{state.count}</h1>
      <h1>{state.name}</h1>
    </div>
  );
}

export default MyUseReducer;
