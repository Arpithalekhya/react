import React, { useContext } from "react";

// function Parent() {
//   const name = "Sachin";
//   return (
//     <div>
//       <A n={name} />
//     </div>
//   );
// }

// const A = (data) => {
//   return (
//     <div>
//       <B name={data.n} />
//       <h1>I'm B....{data.n}</h1>
//     </div>
//   );
// };

// const B = ({ name }) => {
//   return (
//     <div>
//       <C myName={name} />
//     </div>
//   );
// };
// const C = ({ myName }) => {
//   return <div>I'm C....{myName}</div>;
// };
const ct = React.createContext();

const Parent = () => {
  return (
    <div>
      <A />
    </div>
  );
};
const A = () => {
  const myData = "Hyderabad";
  return (
    <div>
      <ct.Provider value={myData}>
        <B />
      </ct.Provider>
    </div>
  );
};
const B = () => {
  return (
    <div>
      <C />
    </div>
  );
};
const C = () => {
  return (
    <div>
      <D />
      I'm C<ct.Consumer>{(data) => <h1>{data}</h1>}</ct.Consumer>
    </div>
  );
};
const D = () => {
  const data = useContext(ct);
  return (
    <div>
      <h1>I'm D:...</h1>
      {data}
    </div>
  );
};
export default Parent;
