import React, { Component } from "react";
import store from "../Store/store";

class A extends Component {
  onName = () => {
    debugger;
    let name = this.refs.name.value;
    store.dispatch({
      type: "NAME",
      data: name,
    });
  };
  render() {
    return (
      <p>
        <input ref="name" />
        <button onClick={this.onName}>Name</button>
      </p>
    );
  }
}

export default A;
