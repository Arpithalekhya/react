import React, {useState, memo} from "react";

function ReactMemo(props) {
  alert("my component called");
  const [name, setName] = useState("Sachin");
  const fnClick = () => {
    setName("Sachin");
  };
  return (
    <div>
      <button onClick={fnClick}>Click me</button>
      <h1>{name}</h1>
    </div>
  );
}

export default memo(ReactMemo);
