import { combineReducers } from "redux";
import { initialData } from "../Utils/init";

export const myReducer = (state = initialData, action) => {
  debugger;
  console.log(state, "reducer state");
  if (action.type === "NAME") {
    debugger;
    return {
      ...state,
      name: action.data,
    };
  }
  if (action.type == "LOC") {
    return {
      ...state,
      location: action.payload,
    };
  }
  if (action.type == "DATA") {
    return {
      ...state,
      data: action.d,
    };
  }
  return state;
};
export const rootReducer = combineReducers({
  m: myReducer,
});
